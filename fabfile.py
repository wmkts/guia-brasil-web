# PASSOS PARA CRIAR UM PROJETO
# 1. CRIE O SERVER E ADICIONE A CHAVE SSH NO REPOSITORIO GIT
# 1. CRIE O PROJETO NO PyCharm COM SEU VIRTUAL ENV
# 2. COPIE O SCRIPT FABFILE.PY PARA A RAIZ DO REPOSITORIO
# 3. EXECUTE O COMANDO fab init_pjr

# fabfile for building, packaging and deploying my app

import os
from fabric.api import cd, env, local, put, run, settings, sudo, task
from fabric.context_managers import prefix
from fabric.contrib.console import confirm
from unipath import Path


PROJ_NAME = 'guiabrasil'
REPO_NAME = 'guia-brasil-web'
REPO_DIR = Path(__file__).parent
PROJ_DIR = REPO_DIR.child(PROJ_NAME)                  # Diretorio do Projeto

env.hosts = ['root@104.131.184.126',]
server = ''
usu_srv = 'wmkts'
# env.key_filename = '~/.ssh/mykeyfile'

# SETTINGS FOR SERVER
venv_name = 'guiaenv'                                  #Name of Virtual Env project
env.venvs_dir = '/home/'+usu_srv+'/virtualenvs/'       #Directory for virtualenv environment
env.activate = 'source ' + os.path.join(env.venvs_dir + venv_name, 'bin/activate')

# SETTINGS FOR LOCAL
envlocal = 'source ' + os.path.join(REPO_DIR +"/"+venv_name, 'bin/activate')

env.srv_prod_dir = '/home/'+usu_srv+'/prod/'
env.srv_temp_dir = '/home/'+usu_srv+'/temp/'
env.srv_stag_dir = '/home/'+usu_srv+'/staging/'

# SETTINGS FOR DB SERVER
env.db_user = 'guiabrasil'                             #Username/DB name for installation
env.db_dbname = 'guiabrasil'                               #Username/DB name for installation
env.db_postgres_postgres_pwd = ''                      #Password for Postgres user postgres
env.db_postgres_user_pwd = ''                          #Password of user for installation



env.git_url = 'git@bitbucket.org:wmkts/guia-brasil-web.git'

# ##################### COMANDOS LOCAIS DE CONFIGURACAO DE UM NOVO PROJETO ######################

# NOVO REPOSITORIO GIT
@task
def add_git():
    with cd(REPO_DIR):
        local('git init')
        local('git remote add origin ' + env.git_url)

# CLONANDO UM REPOSITORIO GIT PARA O REPOSITORIO LOCAL
@task
def clone():
    with cd(REPO_DIR):
        local('git init')
        local('git clone ' + env.git_url)

# GIT PUSH
@task
def push():
    message = raw_input("Enter a git commit message:  ")
    local("git add . && git commit -m \"%s\"" % message)
    local("git push -u origin master")

# GIT PULL
@task
def pull():
    local_branch = _get_local_branch()
    merge_branch = raw_input("Enter a branch name:  ")
    local('git checkout %s' % merge_branch)
    local('git pull origin %s' % merge_branch)
    local('git merge %s' % local_branch)
    local('git push origin %s' % merge_branch)
    local('git checkout %s' % local_branch)
    local('git merge %s' % merge_branch)

@task
def _get_local_branch():
    with cd(REPO_DIR):
        return local('git rev-parse --abbrev-ref HEAD', capture=True).strip()

# CRIAR E CONFIGURAR BANCO DE DADOS LOCAL PGSQL
@task
def init_loc_db():
    env.db_postgres_user_pwd = raw_input("Digite a senha do susuario banco de dados:  ")
    local("sudo -u postgres psql -c \"CREATE DATABASE " + env.db_dbname + ";\"")
    local("sudo -u postgres psql -c \"CREATE USER " + env.db_user + " WITH PASSWORD '" + env.db_postgres_user_pwd + "';\"")
    local("sudo -u postgres psql -c \"GRANT ALL PRIVILEGES ON DATABASE " + env.db_dbname + " TO " + env.db_postgres_user_pwd + ";\"")



# ##################### COMANDOS REMOTOS ######################

# INSTALAR/CONFIGURAR UM SERVIDOR LNUX DO ZERO
@task
def install_srv():

    # sudo('adduser '+usu_srv)
    # sudo('gpasswd -a '+usu_srv+' sudo')
    # local('ssh-keygen')
    # local('ssh-copy-id '+usu_srv+'@' + server)
    sudo('mkdir ' + env.venvs_dir)                          #local onde ficam todos os virtualenvs
    sudo('mkdir ' + env.srv_temp_dir)
    sudo('mkdir ' + env.srv_stag_dir)
    sudo('mkdir ' + env.srv_prod_dir)

    sudo('apt-get update')
    sudo('apt-get install git python-pip python-dev libpq-dev postgresql postgresql-contrib nginx')
    sudo('pip install virtualenv')

# nao ta funcionando
# CRIAR E CONFIGURAR BANCO DE DADOS PGSQL NO SERVIDOR
@task
def init_srv_db():
    env.db_postgres_user_pwd = raw_input("Digite a senha do susuario banco de dados:  ")
    run("sudo -u postgres psql -c \"CREATE DATABASE " + env.db_dbname + ";\"")
    run("sudo -u postgres psql -c \"CREATE USER " + env.db_user + " WITH PASSWORD '" + env.db_postgres_user_pwd + "';\"")
    run("sudo -u postgres psql -c \"GRANT ALL PRIVILEGES ON DATABASE " + env.db_dbname + " TO " + env.db_user + ";\"")


# CLONAR REPOSITORIO NO SERVIDOR
@task
def clone_srv():
    sudo('mkdir -p ' + env.srv_stag_dir)  #Create our dir structure.
    with cd(env.srv_stag_dir):
        sudo('git clone ' + env.git_url)
        with cd(env.venvs_dir):
            run("virtualenv \"%s\"" % venv_name)
            with prefix(env.activate):
                print("pip install -r " + env.srv_stag_dir + REPO_NAME + "/requirements.txt")
                print("Install Requirements...")
                sudo("pip install -r " + env.srv_stag_dir + REPO_NAME + "/requirements.txt")
                sudo("ufw allow 8000")


# ##################### COMANDOS DE ROTINA ######################

# BUILD DO PROJETO NO AMBIENTE DE HOMOLOGACAO
@task
def dep_release():
    with cd(env.srv_stag_dir + REPO_NAME):
        print("Pulling master from shared repository...")
        run("git pull")
        with prefix(env.activate):
            with prefix('export DJANGO_SETTINGS_MODULE=\"'+PROJ_NAME+'.settings.prod\"'):
                print("Collecting static files...")
                run("python manage.py collectstatic --noinput")

                print("Syncing the database...")
                run("python manage.py makemigrations")

                print("Migrating the database...")
                run("python manage.py migrate")
            run('export DJANGO_SETTINGS_MODULE=\"'+PROJ_NAME+'.settings.prod\"')


@task
def restart():
    # Assumes that you have already deployed your init.d script
    sudo('service my-app restart')


# ##################### COMANDOS INICIAIS ######################

# BUILD APP CUJO PROJETO FOI INICIADO LOCALMENTE
@task
def git_init():
    add_git()
    push()


# CLONA O PROJETO MODELO DO REPOSITORIO
@task
def init_local2():
    clone()
    init_loc_db()


# CONFIGURAR SERVIDOR E ATUALIZAR REPOSITORIO DO SERVIDOR
@task
def init_srv():
    install_srv()
    clone_srv()
    # init_srv_db() #nao ta funcionando faze manual


# PUSH DA APLICACAO
@task
def psh():
    push()


# DEPLOY DA APLICACAO
@task
def deploy():
    dep_release()

# run('export DJANGO_SETTINGS_MODULE=\"'+PROJ_NAME+'.settings.production\"')
# export DJANGO_SETTINGS_MODULE="bestworkout.settings.production"
@task
def stash_srv():
    with cd(env.srv_stag_dir + REPO_NAME):
        run("git stash")