from models import Company, City
from rest_framework import serializers
from guiabrasil.core.fields import Base64ImageField


class CompanySerializer(serializers.ModelSerializer):
    thumbnail = Base64ImageField(max_length=None, use_url=True)

    class Meta:
        model = Company
        fields = ('id', 'title', 'sub_title', 'description', 'phone1_ddd', 'phone1', 'phone2_ddd', 'phone2',
                  'business_hours', 'address', 'number', 'complement', 'neighborhood', 'zipcode', 'city',
                  'thumbnail', 'email', 'map_link', 'featured', 'active')


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('name', 'state', 'company_city')