# coding: utf-8
from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings


# This code is triggered whenever a new user has been created and saved to the database

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


state_list = [('AC', 'Acre'), ('AL', 'Alagoas'), ('AM', 'Amazonas'), ('AP', 'Amapá'), ('BA', 'Bahia'), ('CE', 'Ceará'),
              ('DF', 'Distrito Federal'), ('ES', 'Espírito Santo'), ('GO', 'Goiás'), ('MA', 'Maranhão'),
              ('MG', 'Minas Gerais'), ('MS', 'Mato Grosso do Sul'), ('MT', 'Mato Grosso'), ('PA', 'Pará'),
              ('PB', 'Paraíba'), ('PE', 'Pernambuco'), ('PI', 'Piauí'), ('PR', 'Paraná'), ('RJ', 'Rio de Janeiro'),
              ('RN', 'Rio Grande do Norte'), ('RO', 'Rondônia'), ('RR', 'Roraima'), ('RS', 'Rio Grande do Sul'),
              ('SC', 'Santa Catarina'), ('SE', 'Sergipe'), ('SP', 'São Paulo'), ('TO', 'Tocantins')]


class Company(models.Model):
    title = models.CharField(max_length=60)
    sub_title = models.CharField(max_length=60)
    description = models.TextField(blank=True, null=True)
    phone1_ddd = models.CharField(max_length=2)
    phone1 = models.CharField(max_length=60)
    phone2_ddd = models.CharField(max_length=2, blank=True, null=True)
    phone2 = models.CharField(max_length=60, blank=True, null=True)
    business_hours = models.CharField(max_length=60)
    address = models.CharField(max_length=60)
    number = models.CharField(max_length=20)
    complement = models.CharField(max_length=60, blank=True, null=True)
    neighborhood = models.CharField(max_length=30)
    zipcode = models.CharField(max_length=20)
    city = models.ForeignKey('City', related_name='company_city', blank=True, null=True)
    thumbnail = models.ImageField(upload_to='core/company/%Y/%m/', blank=True, null=True)
    email = models.CharField(max_length=100)
    map_link = models.TextField(blank=True, null=True)
    featured = models.BooleanField(_('destauqe'), default=False)
    active = models.BooleanField(_('ativo'), default=True)

    def __unicode__(self):
        return "%s" % (self.title)

    class Meta:
        verbose_name = u'empresa'
        verbose_name_plural = u'empresas'


class City(models.Model):
    name = models.CharField(max_length=65)
    state = models.CharField(_(u'estado'), max_length=65, choices=state_list)

    # state = models.ForeignKey('state', null=True)

    def __unicode__(self):
        return "%s" % (self.name)

    class Meta:
        verbose_name = u'cidade'
        verbose_name_plural = u'cidades'


class State(models.Model):
    uf = models.CharField(max_length=2)
    name = models.CharField(max_length=60)

    def __unicode__(self):
        return "%s" % (self.uf)

    class Meta:
        verbose_name = u'estado'
        verbose_name_plural = u'estados'


class State(models.Model):
    uf = models.CharField(max_length=2)
    name = models.CharField(max_length=60)


class Coupon(models.Model):
    name = models.CharField(max_length=65)
    date_init = models.DateField()
    date_end = models.DateField()

    def __unicode__(self):
        return "%s" % (self.name)